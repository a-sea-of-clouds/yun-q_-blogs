﻿using Blog1._0.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【博客】控制器
    /// </summary>
    public class ArticleController : Controller
    {
        ArticleService article = new ArticleService();
        /// <summary>
        /// 文章列表视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string DPOR369E0zc)
        {
            var ArticleID = DPOR369E0zc == null ? 0 : Convert.ToInt32(Des.DesDecrypt("guo", DPOR369E0zc));
            Admin_ArticleService classService = new Admin_ArticleService();
            LinksService link = new LinksService();
            WebSiteInfo model = new WebSiteInfo();
            Admin_QQUserService quser = new Admin_QQUserService();
            ViewBag.qqusers = quser.GetByQQUserNums();
            ViewBag.MetaKey = model.GetWebSiteInfo().MetaKey;
            ViewBag.SiteTitle = model.GetWebSiteInfo().SiteTitle;
            ViewBag.Address = model.GetWebSiteInfo().Address;
            ViewBag.Links = link.GetCount();

            ViewBag.ClassList = classService.GetTypeList().ToList();//获取文章or标签分类
            ViewBag.DingList = article.GetDingArticle(3);//获取置顶文章
            ViewBag.HotList = article.GetHotArticle(5);//获取热门文章
            ViewBag.UserList = article.GetByQQUserList(12);//获取来访用户信息（默认QQ）

            //博客每页显示数目
            int PageSize = 5;
            ViewBag.PageSize = PageSize;
            //总条数
            int Count = article.GetByQQUserNum();
            ViewBag.Count = Count;

            //总页数
            int PageCount = (Count + PageSize - 1) / PageSize;
            ViewBag.PageCount = PageCount;

            //文章分类Id
            ViewBag.ClassId = ArticleID;
            return View();
        }

        /// <summary>
        /// 文章详情 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(string DPOR369E0zc)
        {
            //参数加密
            var ArticleID = DPOR369E0zc == null ? 0 :  Convert.ToInt32(Des.DesDecrypt("guo", DPOR369E0zc));
            if (ArticleID > 0)
            {
                WebSiteInfo siteInfo = new WebSiteInfo();
                //获取节点key值
                ViewBag.Site = siteInfo.GetWebSiteInfo();
                //增加浏览次数
                var add = article.GetViewAdd(ArticleID);
                //获取文章详情数据
                var model = article.GetArticleInfo(ArticleID).FirstOrDefault();

                //延伸阅读
                ViewBag.OtherList = article.GetRandomArticleList(2);
                //每页显示数目
                int PageSize = 10;
                ViewBag.PageSize = PageSize;

                //总条数
                int Count = 0;
                if (ArticleID == 0)
                { Count = 0; }
                else
                { Count = article.GetComment(ArticleID); }
                ViewBag.Count = Count;
                //总页数
                int PageCount = (Count + PageSize - 1) / PageSize;
                ViewBag.PageCount = PageCount;

                return View(model);
            }
            else
            {
                throw new Exception("请返回上层页面重试！");
            }

        }

        /// <summary>
        /// 文章分页查询
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult LoadArticleByClass(int classId, int page, int pagesize)
        {
            string result = article.GetListByClassId(classId, page, pagesize);
            return Content(result);
        }

        /// <summary>
        /// 文章搜索
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SearchResult(string context)
        {
            var result = article.GetArticleListBySearch(context);
            return Json(result);
        }

        /// <summary>
        /// 加载评论
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult LoadArticleComment(int articleId, int page, int pagesize)
        {
            string result = article.GetFlowArticleComment(articleId, page, pagesize);
            return Content(result);
        }
    }
}