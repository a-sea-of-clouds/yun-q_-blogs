﻿using Blog1._0.Service;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【文章归档】控制器
    /// </summary>
    public class ArchivesController : Controller
    {
        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            WebSiteInfo siteInfo = new WebSiteInfo();
            ArticleService service = new ArticleService();
            ViewBag.SiteTitle = siteInfo.GetWebSiteInfo().SiteTitle;
            ViewData["Year"] = service.Getyear();
            ViewData["ArticleList"] = service.GetAll();
            return View();
        }
    }
}