﻿using Blog1._0.Data;
using Blog1._0.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【评论】控制器
    /// </summary>
    public class CommentController : Controller
    {
        CommentService service = new CommentService();
        /// <summary>
        /// 提交评论
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="articleid"></param>
        /// <param name="editorContent"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddComment(string openid, string articleid, string editorContent)
        {
            var db = new Repository().Base();
            QQUserService services = new QQUserService();
            var userModel = services.GetQQUserInfo(openid).FirstOrDefault();
            if (userModel == null)
            {
                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "非法提交，Openid不存在" });
            }
            if (!userModel.Status.Value)
            {
                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "QQ用户已被锁定，无法评论" });
            }
            WebSiteInfo siteInfo = new WebSiteInfo();
            int maxCommentNum = Convert.ToInt32(siteInfo.GetWebSiteInfo().MaxCommentNum);
            int todayCommentNum = service.GetTodayCommentNum(openid);
            if (todayCommentNum >= maxCommentNum)
            {
                //锁定QQ用户
                userModel.Status = false;
                db.Update(userModel);
                db.Commit();

                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "评论提交失败，已超出每日最大提交数量" });
            }
            CommentModel model = new CommentModel()
            {
                SendId = userModel.Id,
                AcceptId = 0,
                Content = XSS.FilterXSS(editorContent),
                Status = true,
                ParentId = 0,
                ArticleId = Convert.ToInt32(articleid),
                CreateOn = DateTime.Now
            };
            bool result = service.CreateModel(model);

            if (result)
            {
                return Json(new AjaxResult { state = ResultType.success.ToString(), message = "评论提交成功" });
            }
            else
            {
                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "评论提交失败" });
            }
        }

        /// <summary>
        /// 回复评论
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="remarkId"></param>
        /// <param name="targetUserId"></param>
        /// <param name="articleid"></param>
        /// <param name="editorContent"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ReplyComment(string openid, string remarkId, string targetUserId, string articleid, string editorContent)
        {
            var db = new Repository().Base();
            QQUserService services = new QQUserService();
            var userModel = services.GetQQUserInfo(openid).FirstOrDefault();
            if (userModel == null)
            {
                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "非法提交，Openid不存在" });
            }
            if (!userModel.Status.Value)
            {
                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "QQ用户已被锁定，无法评论" });
            }
            WebSiteInfo siteInfo = new WebSiteInfo();
            int maxCommentNum = Convert.ToInt32(siteInfo.GetWebSiteInfo().MaxCommentNum);
            int todayCommentNum = service.GetTodayCommentNum(openid);
            if (todayCommentNum >= maxCommentNum)
            {
                //锁定QQ用户
                userModel.Status = false;
                db.Update(userModel);
                db.Commit();

                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "评论提交失败，已超出每日最大提交数量" });
            }
            CommentModel model = new CommentModel()
            {
                SendId = userModel.Id,
                AcceptId = Convert.ToInt32(targetUserId),
                Content = XSS.FilterXSS(editorContent),
                Status = true,
                ParentId = Convert.ToInt32(remarkId),
                ArticleId = Convert.ToInt32(articleid),
                CreateOn = DateTime.Now
            };
            bool result = service.CreateModel(model);
            if (result)
            {
                return Json(new AjaxResult { state = ResultType.success.ToString(), message = "评论回复成功" });
            }
            else
            {
                return Json(new AjaxResult { state = ResultType.error.ToString(), message = "评论回复失败" });
            }
        }
    }
}