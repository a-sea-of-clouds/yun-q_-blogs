﻿using System.Web;
using System.Web.Mvc;
using Until;

namespace Blog1._0
{
    /// <summary>
    /// 登录验证
    /// </summary>
    public class LoginAuthorizeAttribute : AuthorizeAttribute
    {
        private LoginMode _customMode;
        /// <summary>默认构造</summary>
        /// <param name="Mode">认证模式</param>
        public LoginAuthorizeAttribute(LoginMode Mode)
        {
            _customMode = Mode;
        }
        /// <summary>
        /// 响应前执行登录验证,查看当前用户是否有效 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //登录拦截是否忽略
            if (_customMode == LoginMode.Ignore)
            {
                return;
            }
            var user = HttpContext.Current.Session["Operator"];
            //未登录
            if (user == null)
            {
                ContentResult Content = new ContentResult();
                Content.Content = "<script type='text/javascript'>alert('当前操作信息过期,请重新登录！！！'); top.window.location.href = '/Admin/Login/Test';</script>";
                filterContext.Result = Content;
                return;
            }
            //是否允许登录
            //if (!Current.isAllowLogin && !(user as Operator).IsSystem)
            //{
                
            //    ContentResult Content = new ContentResult();
            //    Content.Content = "<script type='text/javascript'>alert('系统维护中！！！');top.window.location.href = '/';</script>";
            //    filterContext.Result = Content;
            //    return;
            //}
        }
    }
}