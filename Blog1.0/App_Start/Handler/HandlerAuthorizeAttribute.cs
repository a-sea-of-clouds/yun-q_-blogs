﻿using System.Web;
using System.Web.Mvc;
using Until;

namespace Blog1._0
{
    /// <summary>
    /// url验证过滤器
    /// </summary>
    public class HandlerAuthorizeAttribute : ActionFilterAttribute
    {
        private LoginMode _customMode;
        /// <summary>默认构造</summary>
        /// <param name="Mode">认证模式</param>
        public HandlerAuthorizeAttribute(LoginMode Mode)
        {
            _customMode = Mode;
        }
        /// <summary>
        /// 权限认证
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //登录拦截是否忽略
            if (_customMode == LoginMode.Ignore)
            {
                return;
            }
            var user = HttpContext.Current.Session["Operator"];
            //未登录
            if (user == null)
            {
                ContentResult Content = new ContentResult();

                Content.Content = "<script type='text/javascript'>alert('当前操作信息过期,请重新登录！！！'); top.window.location.href = '/';</script>";
                filterContext.Result = Content;
                return;
            }
            //是否超级管理员
            if (Current.GetEntity().IsSystem)
            {
                return;
            }
          
            ////认证执行
            //if (!this.ActionAuthorize(filterContext))
            //{
            //    ContentResult Content = new ContentResult();
            //    Content.Content = "<script type='text/javascript'>alert('很抱歉！您的权限不足，访问被拒绝！');top.Load(false);</script>";
            //    filterContext.Result = Content;
            //    return;
            //}
        }

        /// <summary>
        /// 执行权限认证
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        //private bool ActionAuthorize(ActionExecutingContext filterContext)
        //{
        //    string currentUrl = HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"].ToString();
        //    string path = HttpContext.Current.Request.FilePath;
        //    string con = path.Substring(0,path.LastIndexOf('/') );
        //    return new AuthorizeService().ActionAuthorize(Current.GetEntity().OperatorId, con, currentUrl);
        //}
    }
}