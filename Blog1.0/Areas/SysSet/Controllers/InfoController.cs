﻿using Blog1._0.Service;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.SysSet.Controllers
{
    /// <summary>
    /// 【基本资料】控制器
    /// </summary>
    public class InfoController : MvcControllerBase
    {
        Admin_InfoService service = new Admin_InfoService();
        /// <summary>
        /// 我的基本资料 首页视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion

            ViewBag.MaxFileUpload = Configs.GetValue("MaxFileUpload");
            ViewBag.UploadFileType = Configs.GetValue("UploadFileType");
            var _userId = Current.GetEntity().OperatorId;
            var model = service.GetRoleList(_userId).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 提交更改 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(UserModel model)
        {
            model.UpdateOn = DateTime.Now;
            model.UpdateBy = Current.GetEntity().OperatorId;
            model.Id = Current.GetEntity().OperatorId;
            var result = service.UpdateList(model) ? "操作成功！" : "操作失败！";
            ViewBag.Msg = result;
            return View("Index", model);
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        public JsonResult ExportFile()
        {
            uploadFile _uploadFile = new uploadFile();
            try
            {
                var file = Request.Files[0]; //获取选中文件
                var filecombin = file.FileName.Split('.');
                if (file == null || string.IsNullOrEmpty(file.FileName) || file.ContentLength == 0 || filecombin.Length < 2)
                {
                    _uploadFile.code = -1;
                    _uploadFile.data = new { src = "" };
                    _uploadFile.msg = "上传出错!请检查文件名或文件内容";
                    return Json(_uploadFile);
                }

                string filePathName = string.Empty;
                //定义本地路径位置
                string localPath = Server.MapPath("~/Upload/img/");

                string tmpName = Server.MapPath("~/Upload/img/");
                var tmp = file.FileName;
                //不能上传服务器已有的文件
                if (System.IO.File.Exists(tmpName + tmp))
                {
                    _uploadFile.code = -1;
                    _uploadFile.data = new { src = "" };
                    _uploadFile.msg = "亲，服务器已经有这文件了哟";
                    return Json(_uploadFile);
                }
                #region 相同文件
                // var tmpIndex = 0;
                //判断是否存在相同文件名的文件 相同累加1继续判断  

                //while (System.IO.File.Exists(tmpName + tmp))
                //{

                //    tmp = filecombin[0] + "_" + ++tmpIndex + "." + filecombin[1];
                //} 
                #endregion
                //不带路径的最终文件名  
                filePathName = tmp;

                if (!System.IO.Directory.Exists(localPath))
                    System.IO.Directory.CreateDirectory(localPath);

                file.SaveAs(Path.Combine(tmpName, filePathName));   //保存图片（文件夹）  

                _uploadFile.code = 0;
                //name = Path.GetFileNameWithoutExtension(file.FileName),   // 获取文件名不含后缀名  
                _uploadFile.data = new { src = Path.Combine("/Upload/img/", filePathName) };
                _uploadFile.msg = "上传成功";
                return Json(_uploadFile);
            }
            catch (Exception)
            {
                _uploadFile.code = -1;
                _uploadFile.data = new { src = "" };
                _uploadFile.msg = "上传出错!请检查文件名或文件内容";
                return Json(_uploadFile, JsonRequestBehavior.AllowGet);
            }
        }
    }
}