﻿using Blog1._0.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Permissions.Controllers
{
    public class MenuRoleActionController : MvcControllerBase
    {
        Admin_RoleService service = new Admin_RoleService();

        /// <summary>
        /// 保存角色权限更改
        /// </summary>
        /// <param name="list"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertBatch(IEnumerable<MenuRoleActionModel> list, int menuId)
        {
            var result = service.GetInsertBash(list, menuId) ? SuccessTip() : ErrorTip();
            return Json(result);
        }
    }
}