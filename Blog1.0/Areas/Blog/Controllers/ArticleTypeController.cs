﻿using Blog1._0.Model;
using Blog1._0.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Blog.Controllers
{
    /// <summary>
    /// 【文章类型】控制器
    /// </summary>
    public class ArticleTypeController : MvcControllerBase
    {
        Admin_ArticleTypeService articleClassService = new Admin_ArticleTypeService();
        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            } 
            #endregion
            return View();
        }

        /// <summary>
        /// 新增视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 详情视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = articleClassService.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 修改视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            var model = articleClassService.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult List(Guo_Type filter, PageInfo pageInfo)
        {
            var result = articleClassService.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 新增 方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(ArticleTypeModel model)
        {
            model.CRT_Time = DateTime.Now;
            var result = articleClassService.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 修改方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ArticleTypeModel model)
        {
            var result = articleClassService.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            //删除文章分类，同时删除分类下的文章
            var result = articleClassService.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }
    }
}