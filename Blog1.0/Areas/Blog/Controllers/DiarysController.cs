﻿using Blog1._0.Model;
using Blog1._0.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Blog.Controllers
{
    /// <summary>
    /// 【日记管理】控制器
    /// </summary>
    public class DiarysController : MvcControllerBase
    {
        DiarysService diary = new DiarysService();
        /// <summary>
        /// 日记管理-视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion
            return View();
        }

        /// <summary>
        /// 添加日记 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 修改日记视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            var model = diary.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetDiaysList(Guo_Diays filter, PageInfo pageInfo)
        {
            var result = diary.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增日记
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(DiarysModel model)
        {
            model.CreateOn = DateTime.Now;
            var result = diary.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 日记详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = diary.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 修改日记
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(DiarysModel model)
        {
            var result = diary.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            var result = diary.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

    }
}