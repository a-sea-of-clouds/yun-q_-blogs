﻿$(function () {
    layui.use(['flow'], function () {
        var flow = layui.flow;
        flow.load({
            elem: "#LAY_bloglist",
            done: function (page, next) {
                var pagecount = $(".bloglist").attr("data-pagecount"),
                    type = $(".bloglist").attr("data-type"),
                    pagesize = $(".bloglist").attr("data-pagesize"),
                    lis = [];
                $.ajax({
                    type: "POST",
                    url: "/Resources/LoadResourceByClass",
                    data: {
                        classId: type,
                        page: page,
                        pagesize: pagesize
                    },
                    success: function (res) {
                        //后台拼接返回字符串
                        lis.push(res);
                        next(lis.join(""), page < pagecount);
                    }
                })
            }
        })
    });   
})