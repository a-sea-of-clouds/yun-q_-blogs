﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    /// <summary>
    /// 评论
    /// </summary>
    public partial class Guo_Comment
    {
        public int Id { get; set; }
        /// <summary>
        /// 人员 ID
        /// </summary>
        public int SendId { get; set; }
        /// <summary>
        /// 目标人员ID
        /// </summary>
        public int AcceptId { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> Status { get; set; }
        /// <summary>
        /// 父级ID
        /// </summary>
        public Nullable<int> ParentId { get; set; }
        /// <summary>
        /// 文章ID
        /// </summary>
        public Nullable<int> ArticleId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime CreateOn { get; set; }
    }
}
