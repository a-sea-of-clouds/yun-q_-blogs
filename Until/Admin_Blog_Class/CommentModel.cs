﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// 评论
    /// </summary>
    public class CommentModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 人员ID
        /// </summary>
        public int SendId { get; set; }
        /// <summary>
        /// 人员昵称
        /// </summary>
        public string SendNickName { get; set; }
        /// <summary>
        /// 目标人员ID
        /// </summary>
        public int AcceptId { get; set; }
        /// <summary>
        /// 目标人员昵称
        /// </summary>
        public string AcceptNickName { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool? Status { get; set; }
        /// <summary>
        /// 父ID
        /// </summary>
        public int? ParentId { get; set; }
        /// <summary>
        /// 文章ID
        /// </summary>
        public int? ArticleId { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string ArticleTitle { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreateOn { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadShot { get; set; }
    }
}
