﻿using System;

namespace Until
{
    /// <summary>
    /// 角色管理返回类
    /// </summary>
    public class RoleModel
    {
        public int Id { get; set; }
        /// <summary>
        /// 角色编码
        /// </summary>
        public string RoleCode { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 角色描述
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateOn { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        public int? CreateBy { get; set; }
        /// <summary>
        /// 修改者
        /// </summary>
        public int? UpdateBy { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool? IsActive { get; set; }
    }
}
