﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_AdminMenuMapping : EntityTypeConfiguration<Guo_AdminMenu>
    {
        public Guo_AdminMenuMapping()
        {
            this.ToTable("Guo_AdminMenu");
            this.HasKey(t => t.ID);
        }
    }
}
