﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_DemoMapping : EntityTypeConfiguration<Guo_Demo>
    {
        public Guo_DemoMapping()
        {
            this.ToTable("Guo_Demo");
            this.HasKey(t => t.Id);
        }
    }
}
