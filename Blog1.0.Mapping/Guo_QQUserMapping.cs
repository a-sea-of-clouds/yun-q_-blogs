﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_QQUserMapping : EntityTypeConfiguration<Guo_QQUser>
    {
        public Guo_QQUserMapping()
        {
            this.ToTable("Guo_QQUser");
            this.HasKey(t => t.Id);
        }
    }
}
