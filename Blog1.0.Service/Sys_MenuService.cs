﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    public class Sys_MenuService : Repository
    {
        public string cacheKey = "Menu_{0}";
        public string cacheLabelKey = "Label_{0}";

        /// <summary>
        /// 获取所有一级菜单
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetMenuList()
        {
            var list = this.Base().IQueryable<Guo_Sys_Menu>(t => t.LVL == 1);
            var query = list.Select(t => new MenuInfo
            {
                id = t.MenuID,
                pId = 0,
                name = t.Caption,
                file = t.URL,
                open = false,
                icon = t.icon,
                left = t.LFT,
                right = t.RGT
            });
            return query;
        }

        /// <summary>
        /// 获取当前id下的所有子孙元素  
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetMenuList(int pId)
        {
            var parentMenu = this.Base().FindEntity<Guo_Sys_Menu>(pId);
            var list = this.Base().IQueryable<Guo_Sys_Menu>(t => t.LFT > parentMenu.LFT && t.RGT < parentMenu.RGT);
            var query = from u in list
                        orderby u.MenuID ascending
                        select new MenuInfo
                        {
                            id = u.MenuID,
                            pId = u.Par_ID,
                            // pId = list.Where(t => u.LFT > t.LFT && u.RGT < t.RGT && u.LVL == t.LVL + 1).Count() > 0 ? list.Where(t => u.LFT > t.LFT && u.RGT < t.RGT && u.LVL == t.LVL + 1).FirstOrDefault().MenuID : 0,
                            name = u.Caption,
                            file = u.URL,
                            open = false
                        };
            return query;
        }

        /// <summary>
        /// 获取标签
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetListLabelInfo()
        {
            var list = this.Base().IQueryable<Guo_Catalog>();
            var query = list.Select(t => new MenuInfo
            {
                id = t.CatalogID,
                pId = 0,
                name = t.CatalogName,                
                open = false,                               
            });
            return query;
        }
    }
}
