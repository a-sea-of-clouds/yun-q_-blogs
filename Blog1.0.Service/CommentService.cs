﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【评论】逻辑层
    /// </summary>
    public class CommentService : Repository
    {
        /// <summary>
        /// 获取今日评论数量
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public int GetTodayCommentNum(string openid)
        {
            var db = this.Base();
            var feed = db.IQueryable<Guo_Comment>();
            var user = db.IQueryable<Guo_QQUser>();

            var timeNow = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime time1 = Convert.ToDateTime(timeNow + " 0:00:00");  //数字前记得加空格
            DateTime time2 = Convert.ToDateTime(timeNow + " 23:59:59");

            var query = (from a in
                       (from a in feed
                        join b in user on new { SendId = a.SendId } equals new { SendId = b.Id }
                        where a.CreateOn >= time1 && a.CreateOn <= time2
                        && b.OpenId == openid
                        select new
                        {
                            Column1 = 1,
                            Dummy = "x"
                        })
                         group a by new { a.Dummy } into g
                         select new
                         {
                             feedbackNum = g.Count()
                         }).FirstOrDefault();

            return query != null ? query.feedbackNum : 0;
        }

        /// <summary>
        /// 提交评论
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(CommentModel model)
        {
            var db = this.Base();
            Guo_Comment feed = new Guo_Comment();
            feed.SendId = model.SendId;
            feed.AcceptId = model.AcceptId;
            feed.Content = model.Content;
            feed.Status = model.Status;
            feed.ParentId = model.ParentId;
            feed.ArticleId = model.ArticleId;
            feed.CreateOn = model.CreateOn;
            db.Insert(feed);

            return db.Commit() > 0 ? true : false;
        }
    }
}
