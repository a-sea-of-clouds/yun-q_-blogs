﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【用户管理】逻辑层
    /// </summary>
    public class Admin_QQUserService : Repository
    {
        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(QQUserModel filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var link = db.IQueryable<Guo_QQUser>();
            var query = (from i in link
                         orderby i.CreateOn descending
                         select i).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = link.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<QQUserModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_QQUser>().Where(t => t.Id == Id);
            var query = from i in data
                        select new QQUserModel()
                        {
                            Id = i.Id,
                            NickName = i.NickName,
                            Status = i.Status,
                            OpenId = i.OpenId,
                            Gender = i.Gender,
                            HeadShot = i.HeadShot,
                            Email = i.Email,
                            LastLogin = i.LastLogin,
                            CreateOn = i.CreateOn
                        };

            return query.ToList();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(QQUserModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_QQUser>(t => t.Id == model.Id).FirstOrDefault();
            res.NickName = model.NickName;
            res.Status = model.Status;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_QQUser>(t => t.Id == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_QQUser>(t => t.Id == entity.Id);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }

        /// <summary>
        /// 返回总用户数量
        /// </summary>
        /// <returns></returns>
        public int GetByQQUserNums()
        {
            return this.Base().IQueryable<Guo_QQUser>(t => t.Status == true).Count();
        }
    }
}
