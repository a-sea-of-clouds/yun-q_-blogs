﻿using Blog1._0.Data;
using Blog1._0.Model;
using Until;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【操作管理】逻辑层
    /// </summary>
    public class Admin_ActionService : Repository
    {
        /// <summary>
        /// 操作管理查询
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(Guo_Action filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var diary = db.IQueryable<Guo_Action>();
            var query = from i in diary
                        orderby i.CreateOn descending
                        select new
                        {
                            Id = i.ID,
                            ActionCode = i.ActionCode,
                            ActionName = i.ActionName,
                            Icon = i.Icon,
                            Method = i.Method,
                            OrderBy = i.OrderBy,
                            Status = i.Status,
                            CreateOn = i.CreateOn
                        };

            //操作名称
            if (!string.IsNullOrEmpty(filter.ActionName))
            {
                string actionName = filter.ActionName.ToString();
                query = query.Where(t => t.ActionName.Contains(actionName));
            }

            //状态
            if (filter.Status != null)
            {
                bool isActive = Convert.ToBoolean(filter.Status);
                query = query.Where(t => t.Status == isActive);
            }

            var data = query.Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);//分页
            return new LayuiResult { code = 0, count = diary.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(ActionModel model)
        {
            var db = this.Base();
            Guo_Action diay = new Guo_Action();
            db.SaveChanges();
            diay.ActionName = model.ActionName;
            diay.ActionCode = model.ActionCode;
            diay.Method = model.Method;
            diay.OrderBy = model.OrderBy;
            diay.Icon = model.Icon;
            diay.ClassName = model.ClassName;
            diay.Position = model.Position;
            diay.Remark = model.Remark;
            diay.Status = model.Status;
            diay.CreateBy = Current.GetEntity().OperatorId;
            diay.CreateOn = model.CreateOn;

            db.Insert<Guo_Action>(diay);
            //提交失败，则检查实体类是否加上自增ID标识
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<ActionModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_Action>().Where(t => t.ID == Id);
            var query = from i in data
                        select new ActionModel()
                        {
                            Id = i.ID,
                            ActionName = i.ActionName,
                            ActionCode = i.ActionCode,
                            Method = i.Method,
                            OrderBy = i.OrderBy,
                            Icon = i.Icon,
                            ClassName = i.ClassName,
                            Position = i.Position,
                            Remark = i.Remark,
                            Status = i.Status,
                            CreateOn = i.CreateOn
                        };

            return query.ToList();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(ActionModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_Action>(t => t.ID == model.Id).FirstOrDefault();
            res.ActionName = model.ActionName;
            res.ActionCode = model.ActionCode;
            res.Method = model.Method;
            res.OrderBy = model.OrderBy;
            res.Icon = model.Icon;
            res.ClassName = model.ClassName;
            res.Position = model.Position;
            res.Remark = model.Remark;
            res.Status = model.Status;
            res.UpdateOn = model.UpdateOn;
            res.UpdateBy = model.UpdateBy;
            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                //删除权限时,同时删除菜单权限,菜单角色权限记录
                db.Delete<Guo_Action>(t => t.ID == Id);
                db.Delete<Guo_Menu_Action>(t => t.ActionId == Id);
                db.Delete<Guo_Admin_Role_Menu>(t => t.ActionId == Id);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }
    }
}
